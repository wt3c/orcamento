# coding:utf-8

from django.test import TestCase
from django.contrib.auth.models import User


class Homepage_Test(TestCase):
    def setUp(self):
        self.resp = self.client.get('/')


    def test_get(self):
        """
            GET / deve retornar status code 200. *_*
            Criar rota, view e as templates 404.html, 500.html e index.html
            Adcionar o novo App(central), no settings(INSTALLED_APPS).
        """
        self.assertEqual(200, self.resp.status_code)


class Login_Logout_Test(TestCase):
    def setUp(self):
        self.resp = self.client.get('/login/')


    def test_login(self):
        """
            GET /login deve retornar tela de login.
            GET / se não tiver logado teve redirecionar para tela de login.
        """
        self.assertEqual(200, self.resp.status_code)


    def test_token_csrf(self):
        """
            Todo formsHTML deve ter o token csrf.
        :return:
        """
        self.assertContains(self.resp, 'csrfmiddlewaretoken')


class Login_banco_usuario_Test(TestCase):
    def setUp(self):
        self.user = User(pk=2,
                         username='wcarlos',
                         first_name='welington',
                         last_name='carlos',
                         email='wcarlos3@gmail.com',
                         password='12346',
        )


    def test_create(self):
        self.user.save()
        self.assertEqual(2, self.user.pk)



        # TODO:Testar o LOGOUT
        # def test_logout(self):
        # """
        #         Ainda não sei testar o logout.
        #     :return:
        #     """
        #     self.assertRedirects(self.resp, 'index.html', status_code=200,
        #                           target_status_code=302, msg_prefix='')