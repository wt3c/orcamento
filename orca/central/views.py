from django.shortcuts import render, render_to_response, redirect
from django.contrib.auth import login as auth_login, logout as auth_logout
from django.contrib.auth.forms import AuthenticationForm
# Create your views here.

def homepage(request):
    return render_to_response('index.html')


def login(request):
    if request.method == 'POST':
        form = AuthenticationForm(data=request.POST)

        if form.is_valid():
            auth_login(request, form.get_user())
            return redirect('/')
        else:
            return render(request, "login.html", {"form": form})
    return render(request, 'login.html', {"form": AuthenticationForm()})


def logout(request):
    auth_logout(request)
    return redirect('index.html')

