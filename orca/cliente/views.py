from django.shortcuts import render, render_to_response
from django.http import HttpResponseRedirect
from .forms import ClienteForm


def home_cliente(request):
    """
        A tela inicial do cliente tever ter um campo(s) de pesquisa e botão para
          redirecionar para cadastro e de pesquisar
    """
    return render_to_response("home_cliente.html")

def cadastro(request):
    if request.method == 'POST':
        return gravar(request)
    else:
        return novo(request)

def gravar(request):
    form = ClienteForm(request.POST)
    if not form.is_valid():
        return render(request, 'cadastro.html',
                      {'form': form, })
    form.save()
    return HttpResponseRedirect('cadastro')


def novo(request):
    return render(request, 'cadastro.html',
                              {'form': ClienteForm()})