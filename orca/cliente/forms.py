#coding: utf-8

from django import forms
from django.utils.translation import gettext as _
from django.utils.timezone import now

from orca.cliente.models import Cliente


class ClienteForm(forms.Form):
    nome = forms.CharField(label=_('NOME'), max_length=200, required=True)
    email = forms.EmailField(label=_('EMAIL'), max_length=100, required=False)
    rua = forms.CharField(label=_('ENDEREÇO'), max_length=200, required=False)
    bairro = forms.CharField(label=_('BAIRRO'), max_length=100, required=False)
    cidade = forms.CharField(label=_('CIDADE'), max_length=100, required=False)
    cep = forms.IntegerField(label=_('CEP'), required=False)
    telefone = forms.IntegerField(label=_('TELEFONE'), required=False)
    dt_primeira_visita = forms.DateField(label=_('Data Inicial'), initial=now(), required=False)
    uf = forms.CharField(label=_('UF'), max_length=100, required=False)
    dt_inicial_servico = forms.DateTimeField(label=_('Data de Inicio'), required=False)
    dt_levantamento = forms.DateTimeField(label=_('Data de Levantamento'), required=False)
    dt_orcamento = forms.DateTimeField(label=_('Data do orçamento'), required=False)
    dt_conclusao = forms.DateTimeField(label=_('Data de conclusão'), required=False)
    dt_visita = forms.DateTimeField(label=_('Data de conclusão'), required=False)
    valor_servico = forms.IntegerField(label=_('Valor do serviço'), required=False)
    valor_equipamento = forms.IntegerField(label=_('Valor do equipamento'), required=False)
    url = forms.URLField(label=_('URL(s)'), required=False)
    portas = forms.IntegerField(label=_('Porta logica'), required=False)
    nome_porta = forms.IntegerField(label=_('Nome porta'), required=False)
    servidor_ddns = forms.CharField(label=_('Servidor DDNS'), max_length=100, required=False)
    email_ddns = forms.EmailField(label=_('Email DDNS'), required=False)
    usuario_ddns = forms.CharField(label=_('Usuário DDNS'), max_length=100, required=False)
    senha_ddns = forms.CharField(label=_('Senha DDNS'), max_length=32, widget=forms.PasswordInput, required=False)
    observacao = forms.CharField(label=_('OBS.:'), widget=forms.TextInput, required=False)
    situacao = forms.CharField(label=_('Situação'), max_length=100, required=False)
    solucao = forms.CharField(label=_('Solução'), max_length=100, required=False)
    dvr = forms.CharField(label=_('DVR'), max_length=100, required=False)
    quant_dvr = forms.IntegerField(label=_('Quantidade de DVR(s)'), required=False)
    camera = forms.CharField(label=_('Câmera'), max_length=100, required=False)
    quant_camera = forms.IntegerField(label=_('Quantidade de câmera(s)'), required=False)
    hd = forms.CharField(label=_('HD'), max_length=100, required=False)
    # 'Tipo de cabo UTP ou coaxial'
    cabo = forms.CharField(label=_('Cabo'), max_length=100, required=False)
    metragem = forms.IntegerField(label=_('Metragem'), required=False)
    swicht = forms.CharField(label=_('Swicht'), max_length=100, required=False)
    modem = forms.CharField(label=_('Modem'), max_length=100, required=False)
    outros = forms.CharField(label=_('Outras Informações'), widget=forms.TextInput, required=False)
    tipo_pagamento = forms.CharField(label=_('Tipo de pagamento'), max_length=100, required=False)
    parcela = forms.IntegerField(label=_('Parcela'), required=False)

    def save(self):
        cliente = Cliente(
            nome=self.cleaned_data['nome'],
            email=self.cleaned_data['email'],
            bairro=self.cleaned_data['bairro'],
            cidade=self.cleaned_data['cidade'],
            cep=self.cleaned_data['cep'],
            telefone=self.cleaned_data['telefone'],
            data_primeira_visita=now(),
            endereco=self.cleaned_data['rua'],
        )
        cliente.save()
        return
