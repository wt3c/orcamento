# coding: utf-8

from django.db import models
from django.utils.translation import gettext_lazy as _
from django.utils.timezone import now


class Cliente(models.Model):
    nome = models.CharField(_('NOME'), max_length=200, null=False, blank=False)
    dt_primeira_visita = models.DateTimeField(_('DATA DA PRIMEIRA VISITA'), default=now)

    def __str__(self):
        return self.nome


class Endereco(models.Model):
    rua = models.CharField(_('RUA'), max_length=100, null=False)
    bairro = models.CharField(_('BAIRRO'), max_length=100, null=True)
    cidade = models.CharField(_('CIDADE'), max_length=100, null=True)
    cep = models.IntegerField(_('CEP'), null=True)
    clientes = models.ForeignKey(Cliente, null=False)

    def __str__(self):
        return self.rua


class Contatos(models.Model):
    email = models.EmailField(_('EMAIL'), max_length=100, null=True)
    telefone = models.IntegerField(_('TELEFONE'), null=True)
    clientes = models.ForeignKey(Cliente, null=False)


#TODO: Cria uma soluçã para a senha. Criptografia/Descriptografia
class Web(models.Model):
    url = models.URLField(_('URL'), max_length=100, null=True)
    servidor_ddns = models.CharField(_('SERVIDOR DDNS'), max_length=100, null=True)
    usuario_ddns = models.CharField(_('USUARIO DDNS'), max_length=100, null=True)
    senha_ddns = models.CharField(_('SENHA DDNS'), max_length=100, null=True)
    porta_web = models.IntegerField(_('PORTA WEB'), null=True)
    porta_media = models.IntegerField(_('PORTA MEDIA'), null=True)
    porta_rtsp = models.IntegerField(_('PORTA RTSP'), null=True)
    porta_extra = models.IntegerField(_('PORTA extra'), null=True)
    clientes = models.ForeignKey(Cliente, null=True)

    def __str__(self):
        return self.url


class Orcamento(models.Model):
    dt_levamtamento = models.DateTimeField(_('DATA DO LEVAMTAMENTO'), null=True)
    dt_envio = models.DateTimeField(_('DATA DO ENVIO DO ORÇAMENTO'), null=True)
    dt_inicio_servico = models.DateTimeField(_('DATA DO INICIO DOS SERVIÇOS'), null=True)
    dt_termino = models.DateTimeField(_('DATA DO TERMINO'), null=True)
    vl_inicial_total = models.FloatField(_('VALOR INICIAL TOTAL'), null=True)
    vl_inicial_equipamento = models.FloatField(_('VALOR INICIAL DOS EQUIPAMENTOS'), null=True)
    vl_inicial_servico = models.FloatField(_('VALOR INICIAL DOS SERVIÇOS'), null=True)
    vl_total = models.FloatField(_('VALOR TOTAL'), null=True)
    vl_total_equipamento = models.FloatField(_('VALOR FINAL DE EQUIPAMENTO'), null=True)
    vl_total_servico = models.FloatField(_('VALOR FINAL DOS SERVIÇOS'), null=True)

    TIPO_DE_PAGAMENTO = (
    ('0', 'À VISTA'),
    ('1', 'Crédito 1x'),
    ('2', 'Crédito 2x'),
    ('3', 'Crédito 3x'),
    ('4', 'Crédito 4x'),
    )
    tipo_pagamento = models.CharField(max_length=1, choices=TIPO_DE_PAGAMENTO, default='0')
    clientes = models.ForeignKey(Cliente, null=False)


class Manutencao(models.Model):
    dt_visita = models.DateTimeField(_('DATA DA VISITA'), null=True)
    servico = models.CharField(_('SERVIÇO'), max_length=250, null=True)
    situacao_servico = models.CharField(_('SITUAÇÃO DO SERVIÇO'), max_length=100, null=True)
    clientes = models.ForeignKey(Cliente, null=True)