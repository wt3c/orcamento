# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cliente', '0007_auto_20150521_1851'),
    ]

    operations = [
        migrations.DeleteModel(
            name='Historico',
        ),
        migrations.RemoveField(
            model_name='orcamento',
            name='vl_inicial',
        ),
        migrations.AddField(
            model_name='manutencao',
            name='clientes',
            field=models.ForeignKey(to='cliente.Cliente', null=True),
        ),
        migrations.AddField(
            model_name='manutencao',
            name='dt_visita',
            field=models.DateTimeField(verbose_name='DATA DA VISITA', null=True),
        ),
        migrations.AddField(
            model_name='manutencao',
            name='servico',
            field=models.CharField(max_length=250, verbose_name='SERVIÇO', null=True),
        ),
        migrations.AddField(
            model_name='manutencao',
            name='situacao_servico',
            field=models.CharField(max_length=100, verbose_name='SITUAÇÃO DO SERVIÇO', null=True),
        ),
        migrations.AddField(
            model_name='orcamento',
            name='clientes',
            field=models.ForeignKey(to='cliente.Cliente', default=1),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='orcamento',
            name='tipo_pagamento',
            field=models.CharField(max_length=1, default='0', choices=[('0', 'À VISTA'), ('1', 'Crédito 1x'), ('2', 'Crédito 2x'), ('3', 'Crédito 3x'), ('4', 'Crédito 4x')]),
        ),
        migrations.AddField(
            model_name='orcamento',
            name='vl_inicial_equipamento',
            field=models.FloatField(verbose_name='VALOR INICIAL DOS EQUIPAMENTOS', null=True),
        ),
        migrations.AddField(
            model_name='orcamento',
            name='vl_inicial_servico',
            field=models.FloatField(verbose_name='VALOR INICIAL DOS SERVIÇOS', null=True),
        ),
        migrations.AddField(
            model_name='orcamento',
            name='vl_inicial_total',
            field=models.FloatField(verbose_name='VALOR INICIAL TOTAL', null=True),
        ),
        migrations.AddField(
            model_name='orcamento',
            name='vl_total',
            field=models.FloatField(verbose_name='VALOR TOTAL', null=True),
        ),
        migrations.AddField(
            model_name='orcamento',
            name='vl_total_equipamento',
            field=models.FloatField(verbose_name='VALOR FINAL DE EQUIPAMENTO', null=True),
        ),
        migrations.AddField(
            model_name='orcamento',
            name='vl_total_servico',
            field=models.FloatField(verbose_name='VALOR FINAL DOS SERVIÇOS', null=True),
        ),
        migrations.AddField(
            model_name='web',
            name='senha_ddns',
            field=models.CharField(max_length=100, verbose_name='SENHA DDNS', null=True),
        ),
        migrations.AddField(
            model_name='web',
            name='servidor_ddns',
            field=models.CharField(max_length=100, verbose_name='SERVIDOR DDNS', null=True),
        ),
        migrations.AddField(
            model_name='web',
            name='usuario_ddns',
            field=models.CharField(max_length=100, verbose_name='USUARIO DDNS', null=True),
        ),
        migrations.AlterField(
            model_name='orcamento',
            name='dt_envio',
            field=models.DateTimeField(verbose_name='DATA DO ENVIO DO ORÇAMENTO', null=True),
        ),
        migrations.AlterField(
            model_name='orcamento',
            name='dt_inicio_servico',
            field=models.DateTimeField(verbose_name='DATA DO INICIO DOS SERVIÇOS', null=True),
        ),
        migrations.AlterField(
            model_name='orcamento',
            name='dt_levamtamento',
            field=models.DateTimeField(verbose_name='DATA DO LEVAMTAMENTO', null=True),
        ),
        migrations.AlterField(
            model_name='orcamento',
            name='dt_termino',
            field=models.DateTimeField(verbose_name='DATA DO TERMINO', null=True),
        ),
    ]
