# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('cliente', '0002_auto_20141203_1944'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cliente',
            name='data_primeira_visita',
            field=models.DateField(default=datetime.datetime(2014, 12, 3, 19, 48, 9, 806841)),
            preserve_default=True,
        ),
    ]
