# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('cliente', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='cliente',
            name='bairro',
            field=models.CharField(max_length=100, verbose_name='BAIRRO', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='cliente',
            name='cep',
            field=models.IntegerField(verbose_name='CEP', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='cliente',
            name='cidade',
            field=models.CharField(max_length=100, verbose_name='CIDADE', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='cliente',
            name='data_primeira_visita',
            field=models.DateField(default=datetime.datetime(2014, 12, 3, 19, 43, 1, 647846)),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='cliente',
            name='email',
            field=models.EmailField(max_length=100, verbose_name='EMAIL', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='cliente',
            name='endereco',
            field=models.CharField(max_length=200, verbose_name='ENDEREÇO', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='cliente',
            name='nome',
            field=models.CharField(max_length=200, verbose_name='NOME', default=datetime.datetime(2014, 12, 3, 19, 44, 16, 98608, tzinfo=utc)),
            preserve_default=False,
        ),
    ]
