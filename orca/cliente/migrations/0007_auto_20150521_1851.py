# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('cliente', '0006_auto_20150520_1851'),
    ]

    operations = [
        migrations.CreateModel(
            name='Contatos',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('email', models.EmailField(max_length=100, verbose_name='EMAIL', null=True)),
                ('telefone', models.IntegerField(verbose_name='TELEFONE', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Orcamento',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('dt_levamtamento', models.DateTimeField(verbose_name='DATA DO LEVAMTAMENTO')),
                ('dt_envio', models.DateTimeField(verbose_name='DATA DO ENVIO DO ORÇAMENTO')),
                ('dt_inicio_servico', models.DateTimeField(verbose_name='DATA DO INICIO DOS SERVIÇOS')),
                ('dt_termino', models.DateTimeField(verbose_name='DATA DO TERMINO')),
                ('vl_inicial', models.FloatField(verbose_name='')),
            ],
        ),
        migrations.RemoveField(
            model_name='cliente',
            name='data_primeira_visita',
        ),
        migrations.RemoveField(
            model_name='cliente',
            name='enderecos',
        ),
        migrations.RemoveField(
            model_name='cliente',
            name='web',
        ),
        migrations.RemoveField(
            model_name='endereco',
            name='email',
        ),
        migrations.RemoveField(
            model_name='endereco',
            name='telefone',
        ),
        migrations.RemoveField(
            model_name='web',
            name='porta',
        ),
        migrations.AddField(
            model_name='cliente',
            name='dt_primeira_visita',
            field=models.DateTimeField(default=django.utils.timezone.now, verbose_name='DATA DA PRIMEIRA VISITA'),
        ),
        migrations.AddField(
            model_name='endereco',
            name='clientes',
            field=models.ForeignKey(to='cliente.Cliente', null=True),
        ),
        migrations.AddField(
            model_name='web',
            name='clientes',
            field=models.ForeignKey(to='cliente.Cliente', null=True),
        ),
        migrations.AddField(
            model_name='web',
            name='porta_extra',
            field=models.IntegerField(verbose_name='PORTA extra', null=True),
        ),
        migrations.AddField(
            model_name='web',
            name='porta_media',
            field=models.IntegerField(verbose_name='PORTA MEDIA', null=True),
        ),
        migrations.AddField(
            model_name='web',
            name='porta_rtsp',
            field=models.IntegerField(verbose_name='PORTA RTSP', null=True),
        ),
        migrations.AddField(
            model_name='web',
            name='porta_web',
            field=models.IntegerField(verbose_name='PORTA WEB', null=True),
        ),
        migrations.AddField(
            model_name='contatos',
            name='enderecos',
            field=models.ForeignKey(to='cliente.Endereco', null=True),
        ),
    ]
