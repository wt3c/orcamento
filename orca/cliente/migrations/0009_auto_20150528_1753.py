# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cliente', '0008_auto_20150521_1858'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='contatos',
            name='enderecos',
        ),
        migrations.AddField(
            model_name='contatos',
            name='clientes',
            field=models.ForeignKey(to='cliente.Cliente', default=2),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='endereco',
            name='clientes',
            field=models.ForeignKey(to='cliente.Cliente', default=2),
            preserve_default=False,
        ),
    ]
