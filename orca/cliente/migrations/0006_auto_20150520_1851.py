# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cliente', '0005_auto_20150520_1622'),
    ]

    operations = [
        migrations.CreateModel(
            name='Web',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('url', models.URLField(max_length=100, verbose_name='URL', null=True)),
                ('porta', models.IntegerField(verbose_name='PORTA', null=True)),
            ],
        ),
        migrations.DeleteModel(
            name='Informacao',
        ),
        migrations.DeleteModel(
            name='Portas',
        ),
        migrations.DeleteModel(
            name='Url',
        ),
        migrations.RemoveField(
            model_name='cliente',
            name='bairro',
        ),
        migrations.RemoveField(
            model_name='cliente',
            name='cep',
        ),
        migrations.RemoveField(
            model_name='cliente',
            name='cidade',
        ),
        migrations.RemoveField(
            model_name='cliente',
            name='email',
        ),
        migrations.RemoveField(
            model_name='cliente',
            name='endereco',
        ),
        migrations.RemoveField(
            model_name='cliente',
            name='telefone',
        ),
        migrations.AddField(
            model_name='cliente',
            name='enderecos',
            field=models.ForeignKey(null=True, to='cliente.Endereco'),
        ),
        migrations.AddField(
            model_name='endereco',
            name='bairro',
            field=models.CharField(max_length=100, verbose_name='BAIRRO', null=True),
        ),
        migrations.AddField(
            model_name='endereco',
            name='cep',
            field=models.IntegerField(verbose_name='CEP', null=True),
        ),
        migrations.AddField(
            model_name='endereco',
            name='cidade',
            field=models.CharField(max_length=100, verbose_name='CIDADE', null=True),
        ),
        migrations.AddField(
            model_name='endereco',
            name='email',
            field=models.EmailField(max_length=100, verbose_name='EMAIL', null=True),
        ),
        migrations.AddField(
            model_name='endereco',
            name='rua',
            field=models.CharField(max_length=100, verbose_name='RUA', default='TESTE'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='endereco',
            name='telefone',
            field=models.IntegerField(verbose_name='TELEFONE', null=True),
        ),
        migrations.AddField(
            model_name='cliente',
            name='web',
            field=models.ForeignKey(null=True, to='cliente.Web'),
        ),
    ]
