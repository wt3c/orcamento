# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('cliente', '0003_auto_20141203_1948'),
    ]

    operations = [
        migrations.AddField(
            model_name='cliente',
            name='telefone',
            field=models.IntegerField(null=True, verbose_name='TELEFONE'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='cliente',
            name='data_primeira_visita',
            field=models.DateField(default=datetime.datetime(2014, 12, 3, 19, 59, 53, 504734)),
            preserve_default=True,
        ),
    ]
