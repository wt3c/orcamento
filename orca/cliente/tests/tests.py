# coding: utf-8

from django.test import TestCase
from django.core.urlresolvers import resolve as r
from orca.cliente.models import Cliente
from orca.cliente.forms import ClienteForm


"""
    TDD is a discipline
"""


class TestClientHome(TestCase):
    def setUp(self):
        self.resp = self.client.get('/cliente/')

    def test_get(self):
        self.assertEqual(200, self.resp.status_code)


class ClienteCadastroTest(TestCase):
    """
        O Cadastro de Cliente tever ter os sequintes campos
        Nome; **endereço(rua, bairro, cidade, CEP(validar CEP e restaurar informações), telefone, email)
        data da primeira visita, data de levantameto, data de envio de orçamento, data de início de serviço,
        data de conclusão, **pagamento(tipo de pagamento, parcelas, valor serviço, valor equipamento)
        **informações URL(IP(s) do(s) DVR(s), url(s), [portas], servidor ddns, email do dns, usuarios e senhas),
        **manutenção(mecanismo de agendamento), observação, **historico(data visita, situação{problema;manutenção},
         solução), **equipamentos([dvrs], [cameras], [hds], cabo {camera UTP} ,metragem, moden{modelo},
         swicht{quantidade}, [outros]

        ** == tabela
        [] == lista
        {} == observação

    """

    def setUp(self):
        self.resp = self.client.get('/cliente/cadastro/')

    def test_get(self):
        """
            Get /cliente/cadastro de  retornar code status 200.
            Teste inicial que cria url, funçao da view e template.
        """
        self.assertEqual(200, self.resp.status_code)

    def test_token_csrf(self):
        """ TODO template deve ter o csrf token """
        self.assertContains(self.resp, 'csrfmiddlewaretoken')


class TestClienteForm(TestCase):
    def setUp(self):
        self.resp = self.client.get('/cliente/cadastro/')

    def test_campos_form(self):
        """ O context deve retorna os campos do form """
        form = self.resp.context['form']

        self.assertCountEqual(['nome', 'rua', 'bairro', 'cidade', 'uf', 'cep', 'email', 'telefone',
                               'dt_primeira_visita', 'dt_inicial_servico', 'dt_levantamento', 'dt_orcamento',
                               'dt_conclusao', 'tipo_pagamento', 'valor_servico', 'parcela', 'valor_equipamento',
                               'url', 'portas', 'servidor_ddns', 'email_ddns', 'usuario_ddns', 'senha_ddns',
                               'observacao', 'dt_visita', 'situacao', 'solucao', 'dvr', 'camera', 'hd', 'cabo',
                               'metragem', 'modem', 'swicht', 'outros', 'nome_porta', 'quant_dvr',
                               'quant_camera'], form.fields)


class ClientePostTest(TestCase):
    def setUp(self):
        self.data = dict(nome='Welington', email='wcarlos3@gmail.com', bairro='GRAÇAS',
                         cidade='BELFORD-ROXO', cep='gagagag', telefone='212154545421')
        self.resp = self.client.post('cliente/cadastro/', self.data)

    def test_form_save(self):
        self.forms = ClienteForm(self.resp)
        self.save = ClienteForm.save(self.resp)
        self.assertEqual(200, self.resp.status_code)

    def test_must_inform_email_or_phone(self):
        pass

        #
        # def make_validated_form(self, **kwargs):
        #     self.data.update(kwargs)
        #     form = ClienteForm(self.data)
        #     form.is_valid()
        #     return form


class TestClienteBanco(TestCase):
#TODO: Formatar os campos de datas.
#TODO: Cria uma soluçã para a senha. Criptografia/Descriptografia

    def test_de_tabelas(self):
        from orca.cliente.models import (Cliente, Endereco, Web, Manutencao, Orcamento, Contatos)

    def setUp(self):
        self.cliente = Cliente(pk=2,
                               nome='rosangela',
                               dt_primeira_visita='2014-03-12',
                               )

    def test_gravacao_cliente(self):
        self.cliente.save()
        self.assertEqual(2, self.cliente.pk)

    def test_gera_execao_cliente(self):
        self.Cliente.objects.create(nome='')

    def test_levanta_execao(self):
        """ Deve haver uma exeção se o campo Cliente.nome for null """
        self.assertRaises(Exception, self.test_gera_execao)

    def test_endereco_contatos_tem_cliente(self):
        """ Clientes dever poder ter varios contatos{email, telefone, e endereços """
        from orca.cliente.models import Endereco, Contatos

        self.endereco1 = Endereco(pk=100, rua='A', bairro='BB', cidade='CC',
                             cep='261352525', clientes=self.cliente)

        self.endereco2 = Endereco(pk=101, rua='AA', bairro='BBB', cidade='CCC',
                             cep='26113520', clientes=self.cliente)

        self.endereco1.save()
        self.endereco2.save()

        self.assertEqual(100, self.endereco1.pk)
        self.assertTrue(Endereco.objects.filter(pk=101))

        self.assertEqual('rosangela', self.endereco1.clientes.nome)
        self.assertEqual('rosangela', self.endereco2.clientes.nome)

        self.contato1 = Contatos(email='rosa@gmail.com', telefone='12312131',
                                 clientes=self.cliente)
        self.contato2 = Contatos(email='rosangela@gmail.com', telefone='854555753',
                                 clientes=self.cliente)

        self.assertEqual('rosangela',self.contato1.clientes.nome)
        self.assertEqual('rosangela',self.contato2.clientes.nome)

    def test_web_tem_cliente(self):
        from orca.cliente.models import Web

        self.web = Web()

    def test_orcamento_tem_cliente(self):
        pass



"""
    Testes da documentação do Pyrhon 3.3x e 3.4x + Django
"""
import random
class TestSequenceFunctions(TestCase):


    def setUp(self):
        self.seq = list(range(10))

    def test_shuffle(self):
        print(self.seq)
        random.shuffle(self.seq)
        print(self.seq)

        self.seq.sort()
        print(self.seq)

        self.assertEqual(self.seq, list(range(10)))
        self.assertRaises(TypeError, random.shuffle, (1, 2, 3))

    def test_choice(self):
        element = random.choice(self.seq)
        print('element', element)
        self.assertTrue(element in self.seq)

    def test_sample(self):
        with self.assertRaises(ValueError):
            random.sample(self.seq, 20)
        for element in random.sample(self.seq, 5):
            print(element)
            self.assertTrue(element in self.seq)

# class TesteDocsDjango(TestCase):
#     from django.test.utils import setup_test_environment
#     from django.test import Client
#     from django.core.urlresolvers import reverse
#
#     client = Client()
#     response = client.get('/')
#     response.status_code
#     print('**************** :', 'response.status_code', response.status_code)
#     print('**************** :', 'response.content', response.content)
#     print('**************** :', client.get(reverse('cliente:cadastro')))
