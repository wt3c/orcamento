from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'orca.central.views.homepage', name='homepage'),
    url(r'^login/$', 'orca.central.views.login', name='login'),
    url(r'logout/$', 'orca.central.views.logout', name='logout'),

    #Cliente:
    url(r'^cliente/', include('orca.cliente.urls', namespace='cliente')),


    url(r'^admin/', include(admin.site.urls)),
)
